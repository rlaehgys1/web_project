<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use Firebase\JWT\JWT ;  


$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select("SELECT UserID, password from registeruser WHERE Username=?",
									[$username]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User is not founded";
	}else {
	
		if(app('hash')->check($password, $result[0]->password)){
			$loginResult->status = "success";
			
			$payload = [
				'iss' => "registeruser",
				'sub' => $result[0]->UserID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
			
			];
			
			$loginResult->username = $username;
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
			return response()->json($loginResult);
			
		}else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";
            return response()->json($loginResult);
		}
	}
	return response()->json($loginResult);
});



$router->post('/register', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$email = $request->input("email");
	$password = app('hash')->make($request->input("password"));
	
	$query = app('db')->insert('INSERT into REGISTERUSER
					(Username, Email, Password, Role)
					VALUE (?, ?, ?, ?)',
					[ $username,
					  $email,
					  $password,
					  '0'] );
	return "Ok";
	
});

$router->get('/get_user_profile', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select('SELECT * FROM REGISTERUSER
							      WHERE (UserID=?)',
										[$user_id]);
	return response()->json($results);
}]);

$router->post('/movies/maze-runner-the-death-cure', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	$user = app('auth')->user();
	$username = $user->id;
	$text = $request->input("text");
	
	$query = app('db')->insert('INSERT into maze_runner_the_death_cure
					(username, text)
					VALUE (?, ?)',
					[ $username,
					  $text] ); 
	return "Ok";
	
}]);

$router->get('/movies/maze-runner-the-death-cure/comment', function() {
	
	$results = app('db')->select("SELECT REGISTERUSER.Username, text  FROM maze_runner_the_death_cure,REGISTERUSER");
	return response()->json($results);
});








