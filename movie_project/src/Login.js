import './App.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import {
	Redirect
} from "react-router-dom";
function Login(){
    const [username,setUsername]= useState("");
    const [pw,  	setPw]  	= useState("");
    const [isSuccess, setIsSuccess] = useState(false);
    

	useEffect (()=>{
		//UseEffect จะทำงานเมื่อ Component ถูกโหลดมาตอนครั้งแรก
		//เราจะใช้ Check ว่า User Login ไว้หรือยัง  ถ้ายัง ให้ Login แต่ถ้า Login แล้ว ให้วิ่งไปหน้า Member Area เลย
		//alert(sessionStorage.getItem('api_token')==null);
		
		if (sessionStorage.getItem('username_token')!=null) {
			setIsSuccess(false);
		}
		
	}, []);

    function sendLogin(){
            
        axios.post('http://localhost/api/v1/login',
            {
                "username" : username,
                "password" : pw,
            }
            ).then (
                res=> {	
                    
                    if (res.data.status === "success") {
                            alert("Logged In");
                            setIsSuccess(true)
                            sessionStorage.setItem('username_token', res.data.token);
                            sessionStorage.setItem('username', res.data.username);
                            window.location.reload(); 
                    }else {
                            alert ("Cannot Log In");
                            setIsSuccess(false)
                    }
                }
            );
        
    }
	return(
        <center>
            <div className="box">
                <h2 className="btext">Login</h2><br/>
                <h5 className="text2">Username</h5>
                <input type="text" name="username" placeholder="Enter the username" style={{width:"93%"}}     value={username}   onChange={(e)=>{setUsername(e.target.value)}}/>
                <h5 className="text2 spaceText">Password</h5>
                <input type="password" name="password" placeholder="Enter the password" style={{width:"93%"}}     value={pw}   onChange={(e)=>{setPw(e.target.value)}}/><br/><br/>
                <button class="button1 text" name="login_user" type="submit" style={{fontSize: "0.68em",width: "100%",height:"2.6em"}}     onClick={()=>sendLogin()}>LOGIN</button>
                {isSuccess && <Redirect to="/"/> }
            </div>
        </center>
	);
}
export default Login;