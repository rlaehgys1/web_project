import './App.css';
import { Container, Row, Col } from 'react-grid-system';
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';

function MoviePart(props){
    
	return(
        <Container>
            <Row>
                <Col sm={1}>
                    
                </Col>
                <Col sm={10}>
                    <center>
                        <div className="box2" style={{width:"95%"}}>
                            <Row>
                                <Col sm={6}>
                                    <img src={props.img} width="60%" className="crop"></img>
                                </Col>
                                <Col sm={6}>
                                    <div className="btext" style={{fontSize:"2em",lineHeight:"1.2em"}}>
                                        {props.name_title}
                                    </div>
                                    <div className="text2" style={{fontSize:"0.9em"}}><br/>
                                        {props.descript}
                                    </div>
                                    <Col sm={12}>
                                        <div className="btext" style={{textAlign:"right"}}>
                                        <Link to={props.name_url}>
                                            More...
                                        </Link>
                                        </div>
                                    </Col>
                                </Col>
                            </Row>
                            
                        </div>
                    </center>
                </Col>
                <Col sm={1}>

                </Col>
            </Row>
        </Container>
	);
}
export default MoviePart;