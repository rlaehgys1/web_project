import "./App.css";
import { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
function Top() {
  const [hasToken, setHasToken] = useState(false);
  const [hasUser, setUser] = useState(false);
  useEffect(() => {
    if (sessionStorage.getItem("username_token") == null) {
      setHasToken(false);
    }else{
      setHasToken(true);
    }

    if (sessionStorage.getItem("username") == null) {
      setUser(false);
    }else{
      setUser(true);
    }
  }, []);
  function logout() {
		//ระบบ Logout ในระบบ Stateless Authentication นั้น ไม่จำเป็นต้องยิง REST API
		//เพียงเคลียร์ token ออกจาก Memory แล้ว redirect กลับหน้า Login เป็นอันเพียงพอ
		sessionStorage.clear();
		setHasToken(false);
		alert("Logged Out");
	}

  return (
    <div className="topBar">
      <div className="text top1"></div>
      {!hasToken && (
        <>
          <Link to="/login">
            <div className="text top">Login</div>
          </Link>
          <div className="text top"> | </div>
          <Link to="/register">
            <div className="text top">Register</div>
          </Link>
        </>
      )}
      {hasUser && ( <>
        <p className="text top">
          {sessionStorage.getItem("username")}
        </p>
      </>
      )
      }
      {hasToken && ( <>
        <Link to="/" className="text top" style={{textDecoration:"none"}} onClick={()=>{ logout() }}> Logout </Link>
      </>
      )
      }
    </div>
  );
}
export default Top;
