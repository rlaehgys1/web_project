import './App.css';
import Top from './Top'
import Head from './Head'
import Menu from './Menu'
import HomePage from './HomePage'
import Login from './Login'
import Register from './Register'
import Movies from './Movies'
import MazeRunner3 from './MazeRunner3'
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';
import { Container, Row, Col } from 'react-grid-system';
import {Jumbotron, Button} from 'react-bootstrap'

function App() {
  
  return (
    <Container>
      <Router>
        <Top/>
        <Row>
          <Col sm={12}>
            <Head/>
          </Col>
        </Row>
        <Row>
          <Col sm={2}>
          <Link to="/" className="text-link">
              <Menu name_menu="HOME"/>
          </Link>
          </Col>
          <Col sm={2}>
          <Link to="/movies" className="text-link">
            <Menu name_menu="MOVIES"/>
          </Link>
          </Col>
          <Col sm={2}>
          <Link to="/" className="text-link">
            <Menu name_menu="SERIES"/>
          </Link>
          </Col>
          <Col sm={2}>
          <Link to="/" className="text-link">
            <Menu name_menu="TV SHOWS"/>
            </Link>
          </Col>
          <Col sm={2}>
          <Link to="/" className="text-link">
            <Menu name_menu="COMMUNITIES"/>
            </Link>
          </Col>
          <Col sm={2}>
          <Link to="/" className="text-link">
            <Menu name_menu="HELPS"/>
            </Link>
          </Col>
        </Row>
      
      <div className="WhiteSpace">
        <Switch>
          <Route exact path="/">
           <HomePage/>
          </Route>

          <Route path="/login">
            <Login/>
          </Route>

          <Route path="/register">
           <Register/>
          </Route>

          <Route path="/movies">
           <Movies/>
          </Route>

          <Route path="/maze-runner-the-death-cure">
				    <MazeRunner3/>
			    </Route>
        </Switch>
      </div>
      
      </Router>
    </Container>
  );
}

export default App;
