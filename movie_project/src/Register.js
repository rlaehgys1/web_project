import './App.css';
import { useState } from 'react';
import axios from 'axios';
import {
	Redirect
} from "react-router-dom";

function Register(){
    const [username,setUsername]= useState("");
    const [email,  	setEmail] 	= useState("");
    const [pw1,  	setPw1] 	= useState("");
    const [pw2,  	setPw2] 	= useState("");
    
    const [isSuccess, setIsSuccess] = useState(false);
    function sendSignup(){
            
        if (pw1 != pw2) {			
            alert ("Password ไม่ตรงกัน กรุณากรอกใหม่");
        }else{
        
            axios.post('http://localhost/api/v1/register',
                {
                    "username" : username,
                    "email" : email,
                    "password" : pw1,
                }
            ).then (
                res=> {	
                    
                    if (res.data == "Ok") {
                            alert("สมัครสำเร็จ");
                            setIsSuccess(true)
                    }else {
                            alert ("ไม่สามารถสมัครได้");
                            setIsSuccess(false)
                    }
                }
            );
                
        }
        
    }
	return(
        <center>
            <div className="box">
                <h2 className="btext">Registration Form</h2>
                <p className="text2" style={{fontSize:"0.73em",lineHeight:"1.2em"}}>กรุณากรอกรายละเอียดลงในช่องต่าง ๆ ให้ครบถ้วน</p><br/>
                <h5 className="text2">Username</h5>
                <input type="text" name="username" placeholder="Enter the username" style={{width:"93%"}}     value={username}   onChange={(e)=>{setUsername(e.target.value)}}/>
                <h5 className="text2 spaceText">Email</h5>
                <input type="text" name="email" placeholder="Enter your email" style={{width:"93%"}}    value={email}   onChange={(e)=>{setEmail(e.target.value)}}/>
                <h5 className="text2 spaceText">Password</h5>
                <input type="password" name="password" placeholder="Enter the password" style={{width:"93%"}}     value={pw1}   onChange={(e)=>{setPw1(e.target.value)}}/>
                <h5 className="text2 spaceText">Confirm Password</h5>
                <input type="password" name="password_2" placeholder="Re-enter the password" style={{width:"93%"}}     value={pw2}   onChange={(e)=>{setPw2(e.target.value)}}/><br/><br/>
                <button class="button1 text" name="login_user" type="submit" style={{fontSize: "0.68em",width: "100%",height:"2.6em"}}     onClick={()=>sendSignup()}>SUBMIT</button>
                <p class="text2 spaceText" style={{fontSize:"0.65em"}}>เป็นสมาชิกอยู่แล้ว? <a href="./login" style={{textDecoration:"none"}}>ล็อกอินเลย</a></p>
                {isSuccess && <Redirect to="/" /> }
            </div>
        </center>
	);
}
export default Register;