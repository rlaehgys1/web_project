import "./App.css";
import { useState, useEffect } from "react";
import axios from "axios";
import { Container, Row, Col } from "react-grid-system";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { Redirect } from "react-router-dom";

function MazeRunner3() {
  const [hasUser, setUser] = useState(false);
  const [comment, setComment] = useState("");
  const [isSuccess, setIsSuccess] = useState(true);
  const [commentList, setCommentList] = useState([]);

  useEffect(() => {
    if (sessionStorage.getItem("username") == null) {
      setUser(false);
    }
    axios.get(
      "http://localhost/api/v1/movies/maze-runner-the-death-cure/comment"
    )
      .then((res) => {
        let item_list = [];
        for (let i = 0; i < res.data.length; i++) {
          item_list[i] = [
            res.data[i].Username,
            res.data[i].text,
          ];
        }
        setCommentList(item_list);
      });
  }, []);

  function sendComment() {
    axios
      .post("http://localhost/api/v1/movies/maze-runner-the-death-cure", {
        api_token: sessionStorage.getItem("username_token"),
        text: comment,
      })
      .then((res) => {
        if (res.data == "Ok") {
          alert("Comment Success");
          setIsSuccess(true);
          window.location.reload();
        } else {
          alert("Comment Error");
          setIsSuccess(false);
        }
      }).catch(error => {
        alert("Please login");
        setIsSuccess(false);
    });
  }
  return (
      <>
    <Container>
      <Row>
        <Col sm={12}>
          <div className="box2" style={{ height: "24.5em" }}>
            <Row>
              <Col sm={4}>
                <img
                  src="https://www.crafolic.com/wp-content/uploads/2020/10/the-Maze-Runner3.jpg"
                  width="100%"
                ></img>
              </Col>
              <Col sm={8}>
                <div
                  className="btext"
                  style={{ fontSize: "2em", lineHeight: "1.2em" }}
                >
                  Maze Runner: The Death Cure
                </div>
                <div className="text2" style={{ fontSize: "0.9em" }}>
                  <br />
                  Thomas leads a group of escaped Gladers on their final and
                  most dangerous mission yet. To save their friends, they must
                  break into the legendary Last City, a WCKD-controlled
                  labyrinth that may turn out to be the deadliest maze of all.
                </div>
                <div className="text2" style={{ fontSize: "0.9em" }}>
                  <br />
                  <span style={{ fontWeight: "500" }}>ผู้กำกับ</span> Wes Ball
                  <br />
                  <span style={{ fontWeight: "500" }}>แสดงโดย</span> Dylan
                  O'Brien, Kaya Scodelario, Thomas Brodie-Sangster
                  <br />
                  <span style={{ fontWeight: "500" }}>ประเภท</span>{" "}
                  นิยายวิทยาศาสตร์, ผจญภัย, แอคชัน
                  <br />
                  <span style={{ fontWeight: "500" }}>
                    ผู้อำนวยการสร้าง
                  </span>{" "}
                  Ellen Goldsmith-Vein , Wyck Godfrey, Marty Bowen, Joe Hartwick
                  Jr., Wes Ball, Lee Stollman
                  <br />
                  <span style={{ fontWeight: "500" }}>สตูดิโอ</span> 20th
                  Century Fox
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <br />
      <Row>
        <Col sm={12}>
          <div
            className="box2"
            style={{ height: "24.5em", borderColor: "rgb(200, 200, 200)" }}
          >
            <textarea
              style={{ width: "98%", height: "30%", fontSize: "0.9em" }}
              placeholder="Enter the comment"
              value={comment}
              onChange={(e) => {
                setComment(e.target.value);
              }}
            ></textarea>
            <br />
            <br />
            <button
              class="button1 text"
              name="login_user"
              type="submit"
              style={{ fontSize: "0.68em", width: "100%", height: "2.6em" }}
              onClick={() => sendComment()}
            >
              COMMENT
            </button>
            {!isSuccess && <Redirect to="./login" />}
            {commentList.map((commentItem) => (
              <h4>
                  <br/>
                username : {commentItem[0]} <br/>
                {commentItem[1]}
              </h4>
            ))}
          </div>
        </Col>
      </Row>
    </Container>
    </>
  );
}
export default MazeRunner3;
