import './App.css';
import MazeRunner3 from './MazeRunner3'
import MoviePart from './MoviePart'
import { Container, Row, Col } from 'react-grid-system';
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';

function Movies(){
    
	return(
        <div>
            <MoviePart
                img = "https://www.crafolic.com/wp-content/uploads/2020/10/the-Maze-Runner3.jpg"
                name_title = "Maze Runner: The Death Cure"
                descript = "Thomas leads a group of escaped Gladers on their final and most dangerous mission yet. To save their friends, they must break into the legendary Last City, a WCKD-controlled labyrinth that may turn out to be the deadliest maze of all."
                name_url = "/maze-runner-the-death-cure"
            />
            <br/>
            <MoviePart
                img = "https://images-na.ssl-images-amazon.com/images/I/A1t8xCe9jwL._AC_SL1500_.jpg"
                name_title = "Avengers 3: Infinity War (2018)"
                descript = "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe."
                name_url = "/movies"
            />
            <br/>
            <Switch>
			
		    </Switch>
        </div>
	);
}
export default Movies;